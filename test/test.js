const { assert } = require('chai');
const { newUser } = require('../index.js');

// Test Script
describe('Test newUser object', () => {
	// Test Case
	it('Assert newUser type is an object', () => {
		assert.equal(typeof(newUser), 'object')
	});

	it('Assert newUser email is type string', () => {
		assert.equal(typeof(newUser.email), 'string')
	});

	it('Assert that newUser email is not defined', () => {
		assert.notEqual(typeof(newUser.email), 'undefined')
	});

	it('Assert newUser password is type string', () => {
		assert.equal(typeof(newUser.password), 'string')
	});

	it('Assert newUser password has at least 16 characters', () => {
		assert.isAtLeast(newUser.password.length, 16)
	});


	it('Assert newUser firstName type is a string', () => {
		assert.equal(typeof(newUser.firstName), 'string')
	});

	it('Assert newUser lastName type is a string', () => {
		assert.equal(typeof(newUser.lastName), 'string')
	})

	it('Assert that newUser firstName is not defined', () => {
		assert.notEqual(typeof(newUser.firstName), 'undefined')
	});

	it('Assert that newUser lastName is not defined', () => {
		assert.notEqual(typeof(newUser.lastName), 'undefined')
	});

	it('Assert newUser age has at least 18 characters', () => {
		assert.isAtLeast(newUser.age, 18)
	});


	it('Assert that newUser age type is a number', () => {
		assert.equal(typeof(newUser.age), 'number')
	});

	it('Assert that newUser contact number type is a number', () => {
		assert.equal(typeof(newUser.contactNo), 'string')
	});

	it('Assert that newUser batch number type is a number', () => {
		assert.equal(typeof(newUser.batchNo), 'number')
	});

	it('Assert that newUser batch number type is not defined', () => {
		assert.notEqual(typeof(newUser.batchNo), 'undefined')
	});




	
});
